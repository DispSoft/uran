#!/usr/bin/env bash
# start-server.sh

python manage.py migrate --no-input
python manage.py collectstatic --no-input

if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    (cd uran; python manage.py createsuperuser --no-input)
fi
(gunicorn uran.wsgi --user www-data --bind 0.0.0.0:8010 --workers 3) &
nginx -g "daemon off;"