#!/usr/bin/env bash

docker system prune
docker stop $(docker ps -a -q)
docker build https://gitlab.com/DispSoft/uran_fe.git#main -t dock_uran_fe
docker build https://gitlab.com/DispSoft/uran.git#main -t dock_uran
docker run -p 8000:3000 -d dock_uran
docker run -p 8080:3000 -d dock_uran_fe
