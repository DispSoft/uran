from rest_framework import serializers
from .models import Firmware, Devices


class FirmwareSerializer(serializers.ModelSerializer):
    class Meta:
        model = Firmware
        fields = ('id', 'name', 'path', 'offset')


class DevicesSerializer(serializers.ModelSerializer):
    class Meta:
        ordering = ['-pk']
        model = Devices
        fields = ('id', 'mac', 'start_time', 'last_time', 'end_time', 'attempt', 'channel', 'offset', 'fwsize', 'fw', 'downloaded', 'fw_name', 'is_done')
