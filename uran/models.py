from django.db import models
import time


class Firmware(models.Model):
    name = models.CharField(max_length=64)
    path = models.CharField(max_length=1024)
    offset = models.IntegerField(default=0)
    fsize = models.IntegerField(default=100)

    def __str__(self):
        return f"{self.name} | {self.path} | offset = {self.offset:04X} | size = {self.fsize:04X}"


class Devices(models.Model):
    mac = models.CharField(max_length=64)
    start_time = models.IntegerField(default=time.time())
    last_time = models.IntegerField(default=time.time())
    end_time = models.IntegerField(default=time.time())
    attempt = models.IntegerField(default=1)
    channel = models.IntegerField(default=2)
    offset = models.IntegerField()
    fwsize = models.IntegerField()
    is_done = models.IntegerField(default=0)
    fw = models.ForeignKey(
        Firmware,
        on_delete=models.PROTECT,
        blank=False
    )


    @property
    def downloaded(self):
        return int(self.offset * 100 / self.fwsize)

    @property
    def fw_name(self):
        return self.fw.name

    def __str__(self):
        return f"{self.mac} | {self.downloaded} | {self.fw.name}"
