from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from .models import Firmware, Devices
from .serializers import *
import time

@api_view(['GET', 'POST'])
def firmware_list(request):
    if request.method == 'GET':
        data = Firmware.objects.all()

        serializer = FirmwareSerializer(data, context={'request': request}, many=True)

        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = FirmwareSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def device_list(request):
    if request.method == 'GET':
        data = Devices.objects.all()

        serializer = DevicesSerializer(data, context={'request': request}, many=True)

        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DevicesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



'''
{"ch":0, "offset":150}
'''
@api_view(['PUT'])
def device(request, mac):
    print(request.data)
    if request.method == 'PUT':
        dev = None
        js = request.data
        is_done = False

        try:
            fw_name = js['fw']
            try:
                if fw_name == '0000':
                    is_done = True
                else:
                    fw = Firmware.objects.get(name=fw_name)
            except Devices.DoesNotExist:
                return Response({'error': 'unknown fw name'}, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            return Response({'missing': 'ch'}, status=status.HTTP_400_BAD_REQUEST)


        try:
            ch = js['ch']
        except KeyError:
            return Response({'missing': 'ch'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            off = js['offset']
        except KeyError:
            return Response({'missing':'offset'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            dev = Devices.objects.get(mac=mac)
        except Devices.DoesNotExist:
            dev = Devices()
        dev.mac = mac
        dev.last_time = time.time()
        if is_done:
            dev.offset = dev.fw.fsize
            dev.end_time = time.time()
        else:
            dev.offset = off
            dev.fwsize = fw.fsize
            dev.fw_id = fw.pk
        dev.attempt = 1
        dev.channel = ch

        dev.save()

    return Response(status=status.HTTP_201_CREATED)


'''
@api_view(['PUT', 'DELETE'])
def students_detail(request, pk):
    try:
        student = Firmware.objects.get(pk=pk)
    except Student.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        serializer = StudentSerializer(student, data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
'''