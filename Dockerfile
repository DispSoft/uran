# syntax=docker/dockerfile:1

FROM python:3.9-buster

# install nginx
RUN apt-get update && apt-get install nginx vim -y --no-install-recommends
COPY nginx.default /etc/nginx/sites-available/default
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log


# copy source and install dependencies
RUN mkdir -p /opt/app
RUN mkdir -p /opt/app/pip_cache
RUN mkdir -p /opt/app/uran
COPY req.txt start-server.sh /opt/app/
#COPY .pip_cache /opt/app/pip_cache/
COPY uran /opt/app/uran/
COPY manage.py /opt/app/
WORKDIR /opt/app
COPY db.sqlite3 /opt/app/
RUN pip install -r req.txt --cache-dir /opt/app/pip_cache
RUN chown -R www-data:www-data /opt/app

# start server
EXPOSE 3000
STOPSIGNAL SIGTERM
RUN chmod +x /opt/app/start-server.sh
CMD ["/opt/app/start-server.sh"]

#WORKDIR /uran
#COPY requirements.txt requirements.txt
#RUN pip3 install -r requirements.txt
#COPY . .

#CMD ["python3", "manage.py" , "runserver", "0.0.0.0:8080"]




